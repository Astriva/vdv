////NAV CHANGE COLLOR AFTER SCROLING
$(function () {
  $(document).scroll(function () {
	  var $nav = $('.navbar-fixed-top');
	  $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
	});
});




//GO TO TOP
$(function () {

    var offset = 300,
        duration = 500,
        top_section = $('.to-top'),
        toTopButton = $('a.back-to-top');
    // showing and hiding button according to scroll amount (in pixels)
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $(top_section).fadeIn(duration);
        } else {
            $(top_section).fadeOut(duration);

        }
    });

    // activate smooth scroll to top when clicking on the button

    $(toTopButton).click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 700);
    });

});



//FLUENT SCROLLING ANIMATION
// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(':focus')) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });





//OWL CAROUSEL
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    lazyLoad: true,
    responsiveClass: true,
    autoplay: true,
    autoplayTimeout: 10000,
    autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: false,
            loop: true
        }
    }
})


//FULLSCREEN NAV HAMBURGER
$(document).ready(function () {
    $('.nav-icon').click(function () {
        $(this).toggleClass('animate-icon');
        $('.overlay').fadeToggle();
    });
});

$(document).ready(function () {
    $('.overlay').click(function () {
        $('.nav-icon').removeClass('animate-icon');
        $('.overlay').toggle();
    });
});


//LANGUAGE SELECTOR
    $(document).ready(function () {
                enableSelectBoxes();
            });

            function enableSelectBoxes() {
                $('div.selectBox').each(function () {
                    $(this).children('span.selected').html($(this).children('ul.selectOptions').children('li.selectOption:first').html());
                    $('input.price_values').attr('value', $(this).children('ul.selectOptions').children('li.selectOption:first').attr('data-value'));

                    $(this).children('span.selected,span.selectArrow').click(function () {
                        if ($(this).parent().children('ul.selectOptions').css('display') == 'none') {
                            $(this).parent().children('ul.selectOptions').css('display', 'block');
                        } else {
                            $(this).parent().children('ul.selectOptions').css('display', 'none');
                        }
                    });

                    $(this).find('li.selectOption').click(function () {
                        $(this).parent().css('display', 'none');
                        $('input.price_values').attr('value', $(this).attr('data-value'));
                        $(this).parent().siblings('span.selected').html($(this).html());
                    });
                });
            } 